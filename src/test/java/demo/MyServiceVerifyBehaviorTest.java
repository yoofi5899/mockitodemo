package demo;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

public class MyServiceVerifyBehaviorTest {

    @InjectMocks
    private MyService underTest;

    @Mock
    private ExternalService externalService;

    @BeforeEach
    void setUp(){

        MockitoAnnotations.initMocks(this);
    }

    @Test
    void itShouldValidateID(){
        String id = "10";

        when(externalService.getValidationData(anyString())).thenReturn("some data");

        Boolean result = underTest.validate(id);

        assertTrue(result);
        verify(externalService, times(2)).getValidationData(anyString());


    }


}
